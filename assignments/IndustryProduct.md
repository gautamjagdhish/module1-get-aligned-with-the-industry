# P1 - Study industry ready products
## Product 1
**Name** : Tesla Cars  

**Link** : [Autopilot Demo](https://tesla-cdn.thron.com/delivery/public/video/tesla/5242fcf8-366b-45d6-9f84-6afb030b2560/bvlatuR/WEBHD/the_future_of_autopilot)

**Short description** : Tesla produces electric cars with many advanced capabilities and future updates  
The already implemented features on Tesla cars are,  
*Navigate on Autopilot* : Active Guidance from Highway On-Ramp to Off-Ramp  
*Summon* : Automatically retrieve the car to your location from the parking spot  
*Autopark* : Parallel and Perpendicular park with a single touch  
*Auto Lane change* : Automatically change lanes while driving on the highways  
Even though the full auto driving capability is demonstrated by Tesla as seen in the above video, its still pending authorities approval.  

**Combination of features** : 
* Object detection
* Segmentation  

**Provided by** : Tesla
<br>
<br>

## Product 2
**Name** : Google Photos

**Link** : photos.google.com

**Short description** : User can upload unlimited high quality photos in Google Photos cloud storage. Then Google Photos automatically tags the objects, humans, animals,  surroundings, location .etc when the photos are uploaded. Then it groups photos of similar People & Pets, Places and Things.   
Now when the user inputs the search description like 'Tony Stark eating at Mumbai', it searches the photo tags and groupings and outputs all the photos matching the description.  
It can also identify text from the uploaded photos such as documents or IDs.

**Combination of features** :  
* Object detection
* Segmentation
* ID Recognition
* Person Detection  

**Provided by** : Google
