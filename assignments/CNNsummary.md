# CNN Summary
## What is an CNN ?
Convolutional Neural Network(CNN/ConvNet) is an Artificial Neural Network that is popularly used for analysing Images because of its ability to detect patterns

## How is it different from a Multi Layer Perceptron ?
In CNN, there are convolutional layers in the hidden layers which performs convolution operation on the input.  The role of the ConvNet is to reduce the images into a form which is easier to process, without losing features which are critical for getting a good prediction

## Terminology
**Activation Function** :
To allow Neural Networks to learn complex decision boundaries, we apply a nonlinear activation function to some of its layers. 

**ReLU** : Rectified Linear Unit is a type of activation function. It is a piecewise linear function which outputs the input directly if positive, 0 otherwise

**Convolutional layer** :
A convolutional layer contains a set of filters whose parameters need to be learned. The height and weight of the filters are smaller than those of the input volume. Each filter is convolved with the input volume to compute an activation map made of neurons.

**Filter/Kernel** : The element involved in carrying out the convolution operation in the Convolutional Layer is called the Kernel/Filter

**Pooling Layer** :
The pooling layer reduces the number of parameters and computation by down-sampling the representation. The pooling function can be max or average. A pooling layer is usually incorporated between two successive convolutional layers. 

**Kernel size (K)** : width x height of the filter.

**Stride (S)** : Number of pixels that filter shifts over the input matrix after every iteration.

**Zero padding (pad)** : The rows/columns of zeroes that are symmetrically added to all the sides of the input matrix

**Flattening** : Flattening layer converts multi dimensional arrays into a 1D array.

## CNN Classification Structure Example :
![CNN Classification example](https://miro.medium.com/max/1000/1*vkQ0hXDaQv57sALXAJquxA.jpeg)
