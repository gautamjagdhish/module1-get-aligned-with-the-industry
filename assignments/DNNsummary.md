# Deep Neural Networks
## What is an DNN ?
Deep Neural Network is an Artificial Neural Network with hidden layers in between the input and output layers.



## Terminology
**Activation function** : A neural network is comprised of layers of nodes and learns to map examples of inputs to outputs.

For a given node,the inputs are multiplied by the weights in a node and summed together. This value is referred to as the summed activation of the node. The summed activation is then transformed via an activation function and defines the specific output or **activation** of the node

<img src="https://miro.medium.com/max/766/1*1sXY17hFy3cw-5u7mIcaAg.png" alt="Activation Function" width="500"/>

Depending on the use case, different activation functions are used in different Neural Networks

<img src="https://miro.medium.com/max/1200/1*ZafDv3VUm60Eh10OeJu1vw.png" alt="Types of Activation Functions" width =500>

**Cost Function** : It is a function that measures the performance of a model for given data. Cost Function quantifies the error between predicted values and expected values and presents it in the form of a single real number.

**Gradient Descent** : Gradient descent is an optimization algorithm used to find the values of parameters (coefficients) of a function (f) that minimizes a cost function (cost).

**Back Propogation** : Back-propagation is the practice of fine-tuning the weights of a neural net based on the error rate (i.e. loss) obtained in the previous epoch (i.e. iteration). 
It is just a way of propagating the total loss back into the neural network to know how much of the loss every node is responsible for, and subsequently updating the weights in such a way that minimizes the loss by giving the nodes with higher error rates lower weights and vice versa.